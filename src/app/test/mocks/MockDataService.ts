import { EventEmitter } from "@angular/core";
import { Book } from "src/app/models/Book";
import { DataInterfaceService } from "src/app/services/data-interface.service";

export class MockDataService implements DataInterfaceService {
  books = new Array<Book>()
  bookAddedEvent = new EventEmitter<Book>()
  bookDeletedEvent = new EventEmitter<Book>()


  addBook(b:Book) {
    // throw new Error("Method not implemented.");
    this.bookAddedEvent.emit(b)
  }

  deleteBook() {
    // throw new Error("Method not implemented.");
  }

}
