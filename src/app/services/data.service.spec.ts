import { TestBed } from '@angular/core/testing';
import { Book } from '../models/Book';

import { DataService } from './data.service';

describe('DataService', () => {
  let service: DataService;
  let b: Book;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DataService);
    b = new Book('hi', 'hi', 1)
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('addBook() increases size of books:Array', ()=> {
    service.addBook(b)  //dataservice creates 3 records when it's constructed
    expect(service.books.length).toEqual(4) // ==      VALUE
    expect(service.books.length).toBe(4)    // ===    OBJECT (memory location)
  })

  it('some dumb test', ()=> {
    expect(10 % 2).toEqual(0)
  })

  it('check evenEmitter is firing an event when dataService.addBook(b) is called', () => {
    spyOn(service.bookAddedEvent, 'emit')
    service.addBook(b)
    expect(service.bookAddedEvent.emit).toHaveBeenCalledWith(b)

  })


});
