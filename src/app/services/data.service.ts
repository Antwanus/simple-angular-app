import { EventEmitter, Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { Book } from '../models/Book';

export interface DataInterfaceService {
  books:Array<Book>
  bookAddedEvent:EventEmitter<Book>
  bookDeletedEvent:EventEmitter<Book>

  addBook(b:Book):void
  deleteBook():void
}

@Injectable({
  providedIn: 'root'
})
export class DataService implements DataInterfaceService {
  books:Array<Book>
  bookDeletedEvent = new EventEmitter<Book>()
  bookAddedEvent = new EventEmitter<Book>()   // wrapper on Subject, from rxjs (Angular advises to use EventEmitter)
  // bookAddedEvent2 = new Subject<Book>()       // rxjs

  constructor() {
    this.books = new Array<Book>()
    const book1 = new Book("title1", "author1", 1)
    const book2 = new Book("title2", "author2", 2)
    const book3 = new Book("title3", "author3", 3)

    this.books.push(book1)
    this.books.push(book2)
    this.books.push(book3)
  }

  deleteBook() {
    if(this.books.length > 0) {
      const deletedBook = this.books.pop()
      this.bookDeletedEvent.emit(deletedBook)
    }
    else {
      this.bookAddedEvent.error('there are no books in the collection')
    }
  }
  addBook(b:Book) {
    if(b.author === 'me') {
      this.bookAddedEvent.error('author = "me" not valid')
    }
    else {
      this.books.push(b)
      this.bookAddedEvent.emit(b)   // emits the event with the containing value
      // this.bookAddedEvent2.next(b)  // same as emit() but in rxjs
    }
  }

}
