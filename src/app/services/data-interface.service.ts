import { EventEmitter } from "@angular/core";
import { Book } from "../models/Book";

export interface DataInterfaceService {
  books:Array<Book>
  bookAddedEvent:EventEmitter<Book>
  bookDeletedEvent:EventEmitter<Book>

  addBook(b:Book):void
  deleteBook():void
}
