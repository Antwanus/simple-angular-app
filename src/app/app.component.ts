import { Component, OnInit, ViewChild } from '@angular/core';
import { FooterComponent } from './main/footer/footer.component';
import { Page2Component } from './pages/page2/page2.component';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  title = 'simple-angular-app';

  @ViewChild('footer')
  footerComponent!: FooterComponent;
  @ViewChild('page2')
  page2Component!:Page2Component;

  startTime:string;
  currentPage:number = 1;

  constructor() {
    this.startTime = new Date().toString();
  }

  updateLastAccessed() {
    console.log("previous lastAccessed = ", this.footerComponent.lastAccessed);
    this.footerComponent.lastAccessed = new Date().toString();
  }
  incrementHitCounter(pageNumber:number) {
    this.currentPage = pageNumber;
    if (pageNumber === 2) {
      this.page2Component.incrementHitCounter();
    }
  }

}
