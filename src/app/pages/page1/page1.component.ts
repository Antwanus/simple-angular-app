import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Book } from 'src/app/models/Book';
import { DataInterfaceService } from 'src/app/services/data-interface.service';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-page1',
  templateUrl: './page1.component.html',
  styleUrls: ['./page1.component.css']
})
export class Page1Component implements OnInit, OnDestroy {
  pageName = 'Page 1'
  numberOfBooksWrittenByKhalid:number = 0
  books:Array<Book>

  subscription:Subscription
  subscription2:Subscription

  constructor(
    @Inject('DataInterfaceService')
    private dataService:DataInterfaceService
  ) {
    this.books = dataService.books
    console.log(this.books)

    this.subscription = this.dataService.bookAddedEvent.subscribe(
      (newBook) => {  // onNext
        if(newBook.author === 'DJ Khalid') {
          this.numberOfBooksWrittenByKhalid++
        }
      },
      (error)=> {   // onError
        console.log('An error has occured: ',error)

      },
      ()=> { /* onComplete */ }
    )

    this.subscription2 = this.dataService.bookDeletedEvent.subscribe(
      (next)=> {
        if(next.author === 'DJ Khalid') {
          this.numberOfBooksWrittenByKhalid--
        }
      },
      (error)=> {
        console.log('An error has occured', error);
      },
      ()=>{}
    )
  }

  ngOnInit(): void {
    setTimeout( () => {
      this.pageName = 'First Page'
    }, 5000);

    this.numberOfBooksWrittenByKhalid = this.books.filter(it=> it.author === 'author1').length
  }
  ngOnDestroy():void {
    this.subscription.unsubscribe()
    this.subscription2.unsubscribe()
  }


  onButtonClicked(): void {
    alert(new Date());
  }

}
