import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Book } from 'src/app/models/Book';
import { DataService } from 'src/app/services/data.service';
import { MockDataService } from 'src/app/test/mocks/MockDataService';

import { Page1Component } from './page1.component';

describe('Page1Component', () => {
  let component: Page1Component;
  let fixture: ComponentFixture<Page1Component>;
  let b: Book

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Page1Component ],
      providers: [ {provide:'DataInterfaceService', useExisting:DataService} ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Page1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
    b = new Book('hopla', 'DJ Khalid', 3.50);
  });


  it('check number of books written by {DJ Khalid} is incrementing correctly', ()=> {
    const startValue = component.numberOfBooksWrittenByKhalid
    // console.log(fixture)
    // console.log(fixture.debugElement)
    // console.log(fixture.debugElement.injector) // no reference in the debugger as far as I can see in Chrome..
    const service:DataService = fixture.debugElement.injector.get(DataService)
    service.addBook(b)
    expect(component.numberOfBooksWrittenByKhalid).toEqual(startValue+1)
  })

  it('injecting MockDataService', ()=> {
    const startValue = component.numberOfBooksWrittenByKhalid

    var service = new MockDataService()
    component = new Page1Component(service)

    component.ngOnInit()

    service.addBook(b)
    expect(component.numberOfBooksWrittenByKhalid).toEqual(startValue+1)
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
