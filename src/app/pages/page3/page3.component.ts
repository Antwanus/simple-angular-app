import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { Book } from 'src/app/models/Book';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-page3',
  templateUrl: './page3.component.html',
  styleUrls: ['./page3.component.css']
})
export class Page3Component implements OnInit, OnDestroy {

  books:Array<Book>
  subscription:Subscription

  constructor(private dataService:DataService) {
    this.books = dataService.books

    this.subscription = dataService.bookDeletedEvent.subscribe(
      (next)=>{
        alert(`The book called ${next.title} is being emitted for deletion`)
      },
      (error)=>{
        alert(`No book has been deleted.. ErrorMessage: ${error}`)
      }
    )
  }

  ngOnInit(): void {  }
  ngOnDestroy():void {
    this.subscription.unsubscribe()
  }

  deleteLastBook() {
    this.dataService.deleteBook()
  }

}
