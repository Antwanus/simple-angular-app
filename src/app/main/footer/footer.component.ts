import { Component, Input, OnInit } from '@angular/core';
import { Book } from 'src/app/models/Book';
import { DataService } from 'src/app/services/data.service';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  @Input('lastAccessed')  // this value is received from app.component.html
  lastAccessed:String = '';

  books:Array<Book>

  constructor(private dataService:DataService) {
    this.books = dataService.books
  }

  ngOnInit(): void {
  }

  addBook(){
    const b = new Book("Hi", "me", 1)
    this.dataService.addBook(b)
  }
  addBook2(){
    const b = new Book("Hi", "DJ Khalid", 1)
    this.dataService.addBook(b)
  }

}
