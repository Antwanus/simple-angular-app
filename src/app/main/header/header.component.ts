import { Component, OnInit, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Output()
  pageChangedEvent = new EventEmitter<number>();
  pageNumberRequested = 1;

  constructor() { }

  ngOnInit(): void {
  }
  onPageChange(pageNumber:number): void {
    this.pageNumberRequested = pageNumber;
    console.log("Requested page number = "+this.pageNumberRequested);
    this.pageChangedEvent.emit(pageNumber); // event has occurred -> sends pageChangedEvent to <component (pageChangedEvent)="fun()">
  }

}
